"""
from string import ascii_uppercase

from sys import stdin, stdout, stderr

znaky=ascii_lowercase
celkem=0
#text = input("Zadej text: ")
text=stdin.read()

pocet = {}
for znak in ascii_uppercase:
    if znak not in pocet:
        pocet[znak] = 0



for znak in text:
    if znak in pocet:
        pocet[znak] += 1
    else:
        pocet[znak] = 1
 

#pocet={}
for znak in text:
    #znak =unicodedata.normalize("NFKD")
    znak = znak.upper()
    try:
        pocet[znak]+=1
    except KeyError:
        pocet[znak] = 1
    celkem = celkem + 1
   

for k in pocet:
    print(f"{k}: {pocet[k]/celkem * 100:5.1f}%")

print(celkem)
"""

import unicodedata
from sys import stdin, stdout, stderr, argv
import os
from string import ascii_uppercase

if len(argv) > 1:
    filename = argv[1]
    if os.path.isfile(filename):
        stdin = open(filename, "r")

pocet = {}
for znak in ascii_uppercase:
    pocet[znak] = 0


# text = input("zadej text >")
#text = stdin.read()


"""
for znak in text:
    if znak in pocet:
        pocet[znak] += 1
    else:
        pocet[znak] = 1
"""

celkem = 0
while True:
#for znak in stdin.read(1):
    znak = stdin.read(1)
    if znak == '':
        break
    celkem += 1
    znak = unicodedata.normalize("NFKD", znak).encode("ascii", "ignore").decode("ascii")
    znak = znak.upper()  # každý znak se převede na velké písmeno
    if znak in pocet:
        pocet[znak] += 1

print()

#funkce sum
def sum(seznam):
    dohromady = 0
    for i in seznam:
        dohromady+=i
    return dohromady

#funkce max
def max(seznam):
    nej = 0
    for i in seznam:
        if i > nej:
            nej = i
    return nej


celkem = sum(pocet.values())
nejveci = max(pocet.values())
for k, p in pocet.items():
    print(f"{k}: {p:7d} {pocet[k] / celkem * 100:5.1f}% |{50*p//nejveci*'#'}")