import unicodedata
from sys import stdin
from sys import stdout
from os import path

alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

def Rozkoduj ():
    with open("text_pro_rozkodovani.txt", "r") as f:  # otevřeme soubor pro zakódování
        text = ""
        while True:
            char = f.read(1)
            if not char:
                break
            text += char    # soubor si načteme do proměnné text
    
    text = text.upper()   # všechen text si upravíme na velká písmena
    text = unicodedata.normalize("NFKD", text).encode("ascii", "ignore").decode("ascii", "ignore")  # odstraníme ne-ASCII znaky
    
    rozkodovany_text = "" #sem uložíme zakódovaný text
    posun = int(input("Zadej počet znaků o který bude šifra posunutá: ")) # o kolik posuneme celou šifru
    
    for char in text:
        if char >= "A" and char <= "Z":
            position = ord(char) - 65
            new_position = (position - posun) % 26
            rozkodovany_text += chr(new_position + 65) # v proměnné vysledek máme zakódovaný text

    
    with open("rozkodovany_text.txt", "w") as file:
        stdout.write(path.dirname(__file__))
        adr = path.dirname(__file__)
        file.write(rozkodovany_text)
        stdout.write("Konec\n")
    return rozkodovany_text

Rozkoduj()