from generator import get_random_list
from copy import deepcopy

seznam = get_random_list(20)

print(seznam)

def rekurze(list_):
    result = deepcopy(list_)
    mensi = []
    vetsi = []
    pivot = 0
    while len(mensi) <= 1 and len(vetsi) < 1:
        for i in range(len(seznam)):
            i = pivot
            if seznam[i] < seznam[pivot]:
                mensi.append(seznam[i])
            elif seznam[i] > seznam[pivot]:
                vetsi.append(seznam[i])
        return rekurze(mensi) + [pivot] + rekurze(vetsi)
    return result
print(rekurze(seznam))

#rekurze(list_)