from generator import get_random_list
from copy import deepcopy

#zkouška

def bubble_sort(seznam):
    for _ in range(len(seznam)):
        for i in range(len(seznam) - 1):
            if seznam[i] > seznam[i + 1]:
                seznam[i], seznam[i + 1] = seznam[i + 1], seznam[i]
    return seznam

seznam = get_random_list(10)
print("Original list:", seznam)
print("Sorted list:", bubble_sort(seznam))
