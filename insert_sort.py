from generator import get_random_list
from copy import deepcopy

seznam = get_random_list(20)

print(seznam)

index = 0

def insert_sort(list_):
    result = deepcopy(list_)
    i = 1
    while i < len(seznam):
        j = i
        while j > 0:
            if result[j] < result [j-1]:
                result[j], result[j-1] = result[j-1], result[j]
            j -= 1
        i += 1
    return result
print(insert_sort(seznam))

