from sys import stdin

# f = open("myfile.txt", "r")
# nebo
with open("myfile.txt", "r") as f:
    znak = f.read(1)
    triznaky = f.read(3)
    radek = f.readline()
    
    f.seek(-3, 2)
    konec = f.read()

    f.seek(0)
    zbytek = f.read()

print(znak,triznaky, radek, zbytek)

neco = stdin.read()
print(neco)
